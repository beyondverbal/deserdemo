"""
The flask application package.
"""
import os
UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),'static\waves')
VITALITY_URL = 'http://127.0.0.1:5000/vit'
#print UPLOAD_FOLDER
#print os.chdir(os.path.dirname(__file__))
#print os.getcwd()
#r'C:\Users\ariel\Documents\waves\DeserDemo'
ALLOWED_EXTENSIONS = set(['wav'])


from flask import Flask
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['VITALITY_URL'] = VITALITY_URL
#app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS

import DeserDemo.views


