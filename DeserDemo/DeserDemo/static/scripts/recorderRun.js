// Expose globally your audio_context, the recorder instance and audio_stream


var audio_context;
var recorder;
var audio_stream;
var blobInd = 0;

/**
 * Patch the APIs for every browser that supports them and check
 * if getUserMedia is supported on the browser.
 *
 */
function Rec_Initialize() {
    try {
        // Monkeypatch for AudioContext, getUserMedia and URL
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        window.URL = window.URL || window.webkitURL;

        // Store the instance of AudioContext globally
        audio_context = new AudioContext();
        console.log('Audio context is ready !');
        console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
        alert('No web audio support in this browser!');
    }
}

/**
 * Starts the recording process by requesting the access to the microphone.
 * Then, if granted proceed to initialize the library and store the stream.
 *
 * It only stops when the method stopRecording is triggered.
 */
function startRecording() {
    Rec_Initialize();
    // Access the Microphone using the navigator.getUserMedia method to obtain a stream
    navigator.getUserMedia({ audio: true }, function (stream) {
        // Expose the stream to be accessible globally
        audio_stream = stream;
        // Create the MediaStreamSource for the Recorder library
        var input = audio_context.createMediaStreamSource(stream);
        console.log('Media stream succesfully created');

        // Initialize the Recorder Library
        recorder = new Recorder(input);
        console.log('Recorder initialised');

        // Start recording !
        recorder && recorder.record();
        console.log('Recording...');

    }, function (e) {
        console.error('No live audio input: ' + e);
    });
}

/**
 * Stops the recording process. The method expects a callback as first
 * argument (function) executed once the AudioBlob is generated and it
 * receives the same Blob as first argument. The second argument is
 * optional and specifies the format to export the blob either wav or mp3
 */
function stopRecording(callback, AudioFormat) {
    // Stop the recorder instance
    recorder && recorder.stop();
    console.log('Stopped recording.');

    // Stop the getUserMedia Audio Stream !
    audio_stream.getAudioTracks()[0].stop();

    

    // Use the Recorder Library to export the recorder Audio as a .wav file
    // The callback providen in the stop recording method receives the blob
    if (typeof (callback) == "function") {

       
        recorder && recorder.exportWAV(function (blob) {
            blob.name = new Date().toISOString() + '.wav';
            callback(blob);
            
            // Clear the Recorder to start again !
            recorder.clear();
        }, (AudioFormat || "audio/wav"));
    }
}

function UploadBlob(blob, blobInd) {
    var oReq = new XMLHttpRequest();
    audioFile = "mic" + blobInd + ".wav";
    wavesurfer.load(URL.createObjectURL(blob));
    oReq.open("POST", "uploadaudio/mic" + blobInd, true);

    oReq.onload = function (oEvent) {
        console.log('Uploaded');
        document.getElementById("btn-rec").className = 'button stop';
    };
    oReq.onreadystatechange = (e) => {
        if (oReq.responseText) {
            console.log(oReq.responseText);
            var data = JSON.parse(oReq.responseText);
            motipath = data['fn']
            lcol.loopOnSegments(data['list']);
        }

    }
    oReq.send(blob);
}

// Initialize everything once the window loads
window.onload = function () {
    // Prepare and check if requirements are filled
    //Rec_Initialize();

    // Handle on start recording button
    document.getElementById("btn-rec").addEventListener("click", function (e) {
        if (this.className == 'button record') {//"button record"
            startRecording();
            this.className = "button stop";
        }
        else {
            var _AudioFormat = "audio/wav";
            stopRecording(function (AudioBLOB) {
                UploadBlob(AudioBLOB, blobInd);
                blobInd++;
            }, _AudioFormat);
        }

    }, false);

   
};