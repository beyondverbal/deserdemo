"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template,jsonify,request
from DeserDemo import app
import os
from werkzeug.utils import secure_filename
import pandas as pd

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
		vurl = app.config['VITALITY_URL'],
        year=datetime.now().year,
    )

@app.route('/stat',methods=['POST'])
def stat():
	df = pd.read_csv('stat.csv')
	p = df.values.tolist()
	return jsonify(p)

@app.route('/uploadaudio/<blobname>',methods=['POST'])
def uploadaudio(blobname):
	#os.getcwd()
	#datetime.now().strftime("%Y-%m-%d_%H_%M_%S")+".wav"
	fn = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(blobname+'.wav'))
	with open(fn, "wb") as f:
		chunk_size = 4096
		while True:
			chunk = request.stream.read(chunk_size)
			if len(chunk) == 0:
				df = pd.DataFrame(data={
					'astart': [0,3],
					'bend':   [1,5],
					'clabels':['s0','s0'],
					'dconf':[8,8],
					})
				p = df.values.tolist()
				return jsonify({'list':p,'fn':'dd/MotiPath.wav'})
			f.write(chunk)

    #full_stream = request.stream.read()
	return jsonify({'somthing':'wrong'})

@app.route('/updvoiceprint',methods=['POST'])
def updvoiceprint():
   if request.method == 'POST':
       result = request.form
       fname = result['filename']
       uname = result['uname']
   return jsonify({"ok":"ok"})

@app.route('/des',methods=['POST','GET'])
def des():
	df = pd.DataFrame(data={
		'astart': [0,27,4,0,11,14,10,12,14],
		'bend':   [22,30,6,11,13,15,12,13,15],
		'clabels':['ROD','ROD','s1','s2','s2','s2','s1','s1','s1'],
		'dconf':[8,8,8,10,8,8,8,8,8],
		})
	filePath = request.get_json()['filePath']
	p = df.values.tolist()
	return jsonify({'list':p,'fn':'Users/ariel/source/repos/DeserDemo/DeserDemo/DeserDemo/static/waves/test.wav'})

@app.route('/att',methods=['POST','GET'])
def att():
	df = pd.DataFrame(data={
		'astart': [0,4,10,12,15,17],
		'bend':   [3,6,11,15,16,18],
		'clabels':['s0','no','Ariel','s0','s1','s2'],
		'dconf':[8,8,8,10,8,8],
		})
	
	filePath = request.get_json()['filePath']
	p = df.values.tolist()
	return jsonify({'list':p,'fn':'Users/ariel/source/repos/DeserDemo/DeserDemo/DeserDemo/static/waves/demo.wav'})

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'RecTest.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
