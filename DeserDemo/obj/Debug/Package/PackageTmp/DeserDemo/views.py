"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template,jsonify,request
from DeserDemo import app
import os
from werkzeug.utils import secure_filename
import pandas as pd

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )


@app.route('/uploadaudio',methods=['POST'])
def uploadaudio():
	
	#os.getcwd()
	with open(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(datetime.now().strftime("%Y-%m-%d_%H_%M_%S")+".wav")), "wb") as f:
		chunk_size = 4096
		while True:
			chunk = request.stream.read(chunk_size)
			if len(chunk) == 0:
				return jsonify({'ok':'ok'})
			f.write(chunk)

    #full_stream = request.stream.read()
	return jsonify({'somthing':'wrong'})

@app.route('/updvoiceprint',methods=['POST'])
def updvoiceprint():
   if request.method == 'POST':
       result = request.form
   return jsonify({"ok":"ok"})

@app.route('/des',methods=['POST','GET'])
def des():
	df = pd.DataFrame(data={
		'astart': [0,4,10,12],
		'bend': [3,6,11,15],
		'clabels':['s0','no','m','s0'],
		'dconf':[8,8,8,10],
		})
	filePath = request.get_json()['filePath']
	p = df.values.tolist()
	return jsonify(p)



@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'RecTest.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
