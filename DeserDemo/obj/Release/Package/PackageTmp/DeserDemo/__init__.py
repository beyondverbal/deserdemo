"""
The flask application package.
"""
UPLOAD_FOLDER = r'C:\Users\ariel\Documents\waves\DeserDemo'
ALLOWED_EXTENSIONS = set(['wav'])


from flask import Flask
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS

import DeserDemo.views


